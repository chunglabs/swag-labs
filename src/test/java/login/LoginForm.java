package login;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LoginForm {
    public static Target USERNAME_FIELD = Target.the("username field")
            .located(By.id("user-name"));
    public static Target PASSWORD_FILED = Target.the("password field")
            .located(By.id("password"));
    public static Target LOGIN_BTN = Target.the("Login button")
            .locatedBy("#login_button_container > div > form > input.btn_action");
    public static  Target LOGIN_LABEL = Target.the("Login label")
            .locatedBy("#login_button_container > div > form > h3");
}
