package login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LoginLabel implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(LoginForm.LOGIN_LABEL).viewedBy(actor).asString();
    }

    public static Question<String> displayed(){
        return new LoginLabel();
    }
}
