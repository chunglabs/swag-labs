package stepdefs;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import login.Login;
import login.LoginLabel;
import login.OpenLoginPage;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LoginSteps {

    @Steps
    OpenLoginPage openLoginPage;
    @Steps
    Login login;
    @Before
    public void set_the_stage(){
        OnStage.setTheStage(new OnlineCast());
    }
    @Given("^(.*) open SwagLabs Home Page$")
    public void chung_open_swaglabs_home_page(String actorName) throws Throwable {
        theActorCalled(actorName).wasAbleTo(openLoginPage);
    }

    @When("^He login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void he_login_with_something_and_something(String username, String password) throws Throwable {
        theActorInTheSpotlight().attemptsTo(login.with(username).and(password));
    }

    @Then("^He should see \"([^\"]*)\" message$")
    public void he_should_see_something_message(String message) throws Throwable {
        theActorInTheSpotlight().should(seeThat(LoginLabel.displayed(),equalTo(message)));
    }
}
